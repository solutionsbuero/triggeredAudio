/************************
 * Lib include
*************************/

// include SPI, MP3 and SD libraries
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

// include additional libs for nRF24L01+
#include "RF24.h"


/**********************
 * Config
***********************/

/**************************
 * RADIO CONFIGURATION
 *
 * pair 1
 *  - channel: 60
 *  - pipe: 1node
 *
 * pair 2
 *  - channel: 65
 *  - pipe: 2node
 *
 * pair 3
 *  - channel: 70
 *  - pipe : 3node
 *
 * pair 4
 *  - channel: 75
 *  - pipe: 4node
 *
 **************************/

// Radio Configuration
uint8_t pipe[] = {"4node"};
uint8_t channel = 75;

// Set debug mode
//bool debug = true;


/************************
 * Pin Def
*************************/

// Usual SPI pins, skip...
//#define CLK 13       // SPI Clock, shared with SD card
//#define MISO 12      // Input data, from VS1053/SD card
//#define MOSI 11      // Output data, to VS1053/SD card

// Pin Def Music Maker Shield
#define SHIELD_RESET  -1      // VS1053 reset pin (unused!)
#define SHIELD_CS     7      // VS1053 chip select pin (output) // SS Audio -> 7
#define SHIELD_DCS    6      // VS1053 Data/command select pin (output)
#define CARDCS 4     // Card chip select pin
#define DREQ 3       // VS1053 Data request, ideally an Interrupt pin

// Radio Pins
#define RADIO_CE 10
#define RADIO_CS 9


/********************
 * Object definition
*********************/

Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);

RF24 radio(RADIO_CE, RADIO_CS);


/********************
 * SPI control
*********************/

void switch_to_radio() {
  digitalWrite(SHIELD_CS, HIGH);
  digitalWrite(RADIO_CS, LOW);
}

void switch_to_audio() {
  digitalWrite(RADIO_CS, HIGH);
  digitalWrite(SHIELD_CS, LOW);
}


/********************
 * Player functions
*********************/

void playback_blocking() {
  musicPlayer.playFullFile("/track001.mp3");
}


/********************
 * Radio functions
*********************/

bool check_radio() {
  uint8_t msg = 0;
  radio.read(&msg, sizeof(uint8_t));
  if(msg == 1) {
      Serial.println("Received playback command");
      return true;
  }
  return false;
}


/********************
 * Setup helpers
*********************/

void setup_audio() {
  if (! musicPlayer.begin()) { // initialise the music player
     Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
     while (1);
  }
  Serial.println(F("VS1053 found"));
  if (!SD.begin(CARDCS)) {
    Serial.println(F("SD failed, or not present"));
    while (1);  // don't do anything more
  }

  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(0, 0);

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
}

void setup_radio() {

  // Radio initialization
  radio.begin();
  //radio.setPALevel(power);
  //radio.setDataRate(rate);
  radio.setChannel(channel);
  radio.openReadingPipe(1, pipe);
  radio.startListening();
}


/********************
 * System functions
*********************/

void setup() {

  Serial.begin(9600);
  Serial.println("Serial started");

  // Setup player shield
  setup_audio();

  // Setup radio
  setup_radio();

  /*
  // Play one file, don't return until complete
  Serial.println(F("Playing track 001"));
  musicPlayer.playFullFile("/track001.mp3");
  // Play another file in the background, REQUIRES interrupts!
  Serial.println(F("Playing track 002"));
  musicPlayer.startPlayingFile("/track002.mp3");
  */
}

void loop() {
    if(!musicPlayer.playingMusic) {
        if(check_radio()) {
            switch_to_audio();
            playback_blocking();
            switch_to_radio();
        }
    }
}
