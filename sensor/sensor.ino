/************************
 * Lib include
*************************/

// Radio libs
#include <SPI.h>
#include "RF24.h"
//Doc: https://github.com/nRF24/RF24

// HC-SR04
#include <HCSR04.h>
// Doc: https://github.com/Martinsos/arduino-lib-hc-sr04


/**********************
 * Config
***********************/

/**************************
 * RADIO CONFIGURATION
 *
 * pair 1
 *  - channel: 60
 *  - pipe: 1node
 *
 * pair 2
 *  - channel: 65
 *  - pipe: 2node
 *
 * pair 3
 *  - channel: 70
 *  - pipe : 3node
 *
 * pair 4
 *  - channel: 75
 *  - pipe: 4node
 *
 **************************/

// Radio Configuration
uint8_t pipe[] = {"4node"};
uint8_t channel = 75;

// Downtime: Block trigger after successful triggering (ms)
// To make it more trigger-happy: decrease to 5000
int downtime = 10000;

// Sensing distance: if measurement is SMALLER than this, playback is triggerd, int in cm
// To make it more trigger-happy: increase to 220
int trigger_distance = 150;

// Set debug mode
bool debug = true;


/************************
 * Pin Def
*************************/

// Radio Pins
const byte radio_pins[2] = {
  10, // CE
  9   // CS
};

// Pin def HC-SR04
const byte sensor_pins[2] = {
  2, // trigger
  3  // echo
};


/********************
 * Object definition
*********************/

// Radio object: Params: int CE, int CS
RF24 radio(radio_pins[0], radio_pins[1]);

// Sensor object: Params: int trigger, int echo
UltraSonicDistanceSensor distanceSensor(sensor_pins[0], sensor_pins[1]);

/********************
 * Gobal storage
*********************/

uint8_t last_state = 0;

uint32_t downuntil = 0;

/********************
 * Helper functions
*********************/

uint8_t get_triggered() {
  int distance = distanceSensor.measureDistanceCm();
  if(distance < trigger_distance) {
    return 1;
  }
  else {
    return 0;
  }
}

/********************
 * System functions
*********************/

void setup() {

  // init serial
  if(debug) {
    Serial.begin(9600);
  }

  // Radio initialization
  radio.begin();
  //radio.setPALevel(power);
  //radio.setDataRate(rate);
  radio.setChannel(channel);
  radio.openWritingPipe(pipe);
}

void loop() {

    uint8_t msg;
    uint8_t current_state = get_triggered();
    uint32_t mnow = millis();
    if(debug) {
      Serial.print("current_state: ");
      Serial.println(current_state);
      Serial.print("last_state: ");
      Serial.println(last_state);
      Serial.print("remaining downtime: ");
      Serial.println((downuntil - mnow)/1000);
    }
    if(current_state && !last_state && downuntil < mnow) {
        msg = 1;
        downuntil = mnow + downtime;
    }
    else {
        msg = 0;
    }

    if(debug) {
        Serial.print("Message: ");
        Serial.println(msg);
    }
    radio.write(&msg, sizeof(uint8_t));
    last_state = current_state;
    if(debug) {
      // Make it more trigger-happy: decrease to 150
      delay(200);
    }
}
